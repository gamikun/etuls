# -*- coding: utf-8 -*-
import os, sys, boto, random, subprocess as sproc
from config import *
from datetime import date
from boto.s3.key import Key

if UPLOAD_FILES == True:
  s3 = boto.connect_s3(AWS_KEY, AWS_SEC)
  aws_dbs = s3.get_bucket(S3_BUCKET)

# Se crea la carpeta tmp, en caso de no existir
if not os.path.exists(TMP_DIR):
  os.mkdir(TMP_DIR)
    
# Si existe el archivo del estado corriendo
# no se continua, y se termina el proceso
if not os.path.isfile(TMP_DIR+"/respaldar-db.running"):
 
  # Lo primero que se hace es crear el archivo
  # with open(TMP_DIR+"/respaldar-db.running","w") as filerun:
  #  filerun.write('1')
  
  # Tomar la fecha que se va a utilizar
  # y se deja la cadena ya lista para usarse
  hoy = date.today()
  shoy = "%d-%d-%d-%d" % (hoy.year, hoy.month, hoy.day, random.randint(1,100))
  
  # --------------------------------- #
  # 1. Respaldar bases de datos MySQL #
  # --------------------------------- #
  if len(MYSQL_DBS) > 0:
    print '\n-- MySQL --\n-----'
    for db in MYSQL_DBS:
      print " * " + db
      # 1.1 Hacer el dump de la base de datos y comprimirlo
      dump_name = "%s/%s-%s.sql" % (TMP_DIR, db, shoy)
      os.system('sudo mysqldump "%s" > "%s"' % (db, dump_name))
      os.system('tar zcvf "%s.tgz" "%s"' % (dump_name, dump_name))
      if UPLOAD_FILES == True:
        # 1.2 Enviar los datos
        k = Key(aws_dbs)
        k.key = SERVER_NAME + '/db-mysql/%s-%s.sql.tgz' % (db, shoy)
        k.set_contents_from_filename(dump_name+'.tgz')
      if DELETE_FILES == True:
        # 1.3 Eliminar los archivos generados
        os.remove('%s.tgz' % dump_name)
      os.remove(dump_name)
      
  # --------------------------------- #
  # 2. Respaldar bases de datos Mongo #
  # --------------------------------- #
  if len(MONGO_DBS) > 0:
    print "\n-- MongoDB --\n-----"
    for db in MONGO_DBS:
      print ' * ' + db
      # 2.1 Hacer dump de la base de datos de mongo
      dump_name = "%s/%s-%s.mongo" % (TMP_DIR, db, shoy)
      os.system("sudo mongodump -db '%s' -o '%s'" % (db, dump_name))
      os.system('tar zcvf "%s.tgz" "%s"' % (dump_name, dump_name))
      if UPLOAD_FILES == True:
        # 2.2 Enviar los datos al S3
        k = Key(aws_dbs)
        k.key = 'db-mongo/%s-%s.mongo.tgz' % (db, shoy)
        k.set_contents_from_filename(dump_name+'.tgz')
      if DELETE_FILES == True:
        # 2.3 Eliminar el contenido del dump y el dump
        os.system("rm -rf '%s/'" % dump_name)
        os.remove(dump_name+'.tgz')

  # --------------------------------- #
  # 2. Respaldar bases de datos Mongo #
  # --------------------------------- #
  before_dir = os.getcwd()
  os.chdir(TMP_DIR)
  if len(POSTGRESQL_DBS) > 0:
    print "- PostgreSQL -"
    for db in POSTGRESQL_DBS:
      print ' * ' + db['name']
      # 3.1 Hacer el dumo de PostgreSQL
      dump_name = "./%s-%s.sql" % (db['name'], shoy)
      tempenv = os.environ.copy()
      tempenv['PGPASSWORD'] = db['password']
      p = sproc.Popen(['pg_dump','-h','127.0.0.1','-w','-U',db['user'],'--inserts',db['name']],
        stdout=sproc.PIPE, stderr=sproc.PIPE, env=tempenv)
      salida, error = p.communicate()
      if p.returncode == 0:
        with open(dump_name, 'wb') as dump_file:
          dump_file.write(salida)
        p = sproc.Popen(['tar','zcvf',dump_name+'.tgz',dump_name],
          stdout=sproc.PIPE, stderr=sproc.PIPE)
        p.communicate()
        if p.returncode == 0:
          if UPLOAD_FILES == True:
            # 3.2 Enviar los datos al S3
            k = Key(aws_dbs)
            k.key = 'db-psql/%s-%s.psql.tgz' % (db['name'], shoy)
            k.set_contents_from_filename(dump_name+'.tgz')
          if DELETE_FILES == True:
            # 3.3 Eliminar el contenido del dump y el dump
            os.remove(dump_name+'.tgz')
        os.remove(dump_name)
      else:
        pass # ERROR: no pasa nada

  os.chdir(before_dir)
  # os.remove(TMP_DIR+"/respaldar-db.running")

sys.exit(0)